//
//  SRKAppDelegate.h
//
//  Created by Protools on 7/28/15.
//  Copyright (c) 2015 KUSF.org. All rights reserved.


#import <UIKit/UIKit.h>

@interface SRKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
