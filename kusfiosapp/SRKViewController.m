//
//  SRKViewController.m
//
//  Created by Protools on 7/28/15.
//  Copyright (c) 2015 KUSF.org. All rights reserved.

//
#import "SRKViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad

{
    
    [super viewDidLoad];
    
    NSArray *arrayEntities=[NSArray arrayWithObjects:@"ra:track",@"ra:artist",@"ra:time", nil];
    xmlOp=[[xmlOperation alloc]initWithPostRequest:@"http://kusf.radioactivity.fm/feeds/last25.xml" requestedElements:arrayEntities rootElement:@"item" delegate:self];
    [self embedPlaylist];
 
    
}


-(void)embedPlaylist {
    NSString *embedHTML = @"<iframe width=\"300\" height=\"480\" src=\"https://jayna.usfca.edu/kusf/playlist_feed.php\" frameboarder=\"0\" allowfullscreen></iframe>";
    NSString *html = [NSString stringWithFormat:embedHTML];
    
    [WebView loadHTMLString:html baseURL:nil];
    [self.view addSubview:WebView];
}



#pragma mark - xmlOperationDelegate

-(void)DidFinishXmlOperation:(NSArray *)arrayData{
    
    arrayValues=[[NSMutableArray alloc]init];
    
    if([arrayData count]>0){
        
        
        
        [arrayValues addObjectsFromArray:arrayData];
        
        [tblView reloadData];
        
    }
    
    
    
}

#pragma mark - UITableView Delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [arrayValues count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 25;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{

    
    CustomCell *cell=nil;
    
    [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
    
    cell=(CustomCell *)tblCell;
    
    NSDictionary *dictionaryObject=[arrayValues objectAtIndex:indexPath.row];
    
    lblTitle.text=[dictionaryObject valueForKey:@"ra:track"];
    
    lblArtist.text=[dictionaryObject valueForKey:@"ra:artist"];
    
    lblTime.text=[dictionaryObject valueForKey:@"ra:time"];
    

    return cell;
    
}


-(IBAction)Done:(id)sender{
    
    if([self.presentingViewController respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
        [self.presentingViewController dismissViewControllerAnimated:(YES) completion:nil];
    
        ////nslog(@"Oooops, what system is this ?!!! - should never see this !");
    
}
- (void)didReceiveMemoryWarning

{
    
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    
}



@end

