//
//  ViewController.m
//  webview
//
//  Created by Protools on 7/14/15.
//  Copyright (c) 2015 KUSF. All rights reserved.
//

#import "SRKViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSURL *url = [NSURL URLWithString:@"http://kusf.radioactivity.fm/feeds/lasthour.html"];
    
    NSURLRequest *REQUEST = [NSURLRequest requestWithURL: url];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
