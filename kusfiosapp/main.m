//
//  main.m
//  kusfiosapp
//
//  Created by Protools on 8/11/15.
//  Copyright (c) 2015 KUSF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRKAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SRKAppDelegate class]));
    }
}
